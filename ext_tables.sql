CREATE TABLE tx_maagitcookie_domain_model_category (
    name varchar(255) DEFAULT '' NOT NULL,
    description text DEFAULT '' NOT NULL,
    needed tinyint(1),
    wanted tinyint(1),
	checked tinyint(1),
	services varchar(255) DEFAULT '' NOT NULL
);

CREATE TABLE tx_maagitcookie_domain_model_service (
    name varchar(255) DEFAULT '' NOT NULL,
	category int(11) DEFAULT 0 NOT NULL,
    pattern varchar(255),
	replacement varchar(255)
);