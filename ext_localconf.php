<?php
defined('TYPO3') || die('Access denied.');

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitcookie',
	'Consent',
	[
		\Maagit\Maagitcookie\Controller\ConsentController::class => 'show'
	],
	[
		\Maagit\Maagitcookie\Controller\ConsentController::class => 'show'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
