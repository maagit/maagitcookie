<?php
defined('TYPO3') || die('Access denied.');

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitcookie',																			// extension name
    'Consent',																				// plugin name
    'LLL:EXT:maagitcookie/Resources/Private/Language/locallang.xlf:plugins.title',			// plugin title
    'extensions-maagitcookie_consent',														// icon identifier
    'maagitcookie',  																		// group
    'LLL:EXT:maagitcookie/Resources/Private/Language/locallang.xlf:plugins.description'		// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitcookie/Configuration/FlexForms/Consent.xml',
	$pluginSignature
);
?>