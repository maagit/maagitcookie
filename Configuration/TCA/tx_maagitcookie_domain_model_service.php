<?php
return [
	'ctrl' => [
		'title' => 'Cookieconsent Service',
		'label' => 'name',
		'sortby' => 'sorting',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group'
		],
        'iconfile' => 'EXT:maagitcookie/Resources/Public/Icons/cookieconsent_service.svg',
	 ],
	 'interface' => [
	 
	 ],
     'types' => [
		 '1' => [
			 'showitem' => '
				--div--;Cookieconsent service,
			 	name,
				category,
				pattern,
				replacement,
				--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access'
		]
	 ],
	 'palettes' => [
         'access' => [
             'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
             'showitem' => '
                 starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                 endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
                 --linebreak--,
				 fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel'
		 ]
	 ],
	 'columns' => [
		 'hidden' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
			 'config' => [
				 'type' => 'check',
				 'renderType' => 'checkboxToggle',
				 'items' => [
					 [
						 'label' => 'Visible',
						 'labelChecked' => 'Enabled',
						 'labelUnchecked' => 'Disabled',
						 'invertStateDisplay' => true
					]
				]
			 ]
		 ],
		 'starttime' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			 'config' => [
				 'type' => 'datetime',
				 'default' => 0
			 ],
			 'l10n_mode' => 'exclude',
			 'l10n_display' => 'defaultAsReadonly'
		 ],
		 'endtime' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			 'config' => [
				 'type' => 'datetime',
				 'default' => 0
			 ],
			 'l10n_mode' => 'exclude',
			 'l10n_display' => 'defaultAsReadonly'
		 ],
		 'name' => [
			 'label' => 'Name',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255,
				 'required' => true
			 ]
		 ],
		 'category' => [
			 'label' => 'Category',
			 'config' => [
				 'type' => 'select',
				 'renderType' => 'selectSingle',
				 'foreign_table' => 'tx_maagitcookie_domain_model_category',
				 'foreign_table_where' => 'AND tx_maagitcookie_domain_model_category.hidden = 0 ORDER BY tx_maagitcookie_domain_model_category.sorting',
				 'maxitems' => 1,
				 'required' => true
			]
		 ],
		 'pattern' => [
			 'label' => 'Pattern',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ],
		 'replacement' => [
			 'label' => 'Replacement',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ]
	 ]
];
