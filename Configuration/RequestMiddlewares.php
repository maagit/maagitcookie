<?php
return [
	'frontend' => [
		'maagit/plain-rendering-handler' => [
			'target' => Maagit\Maagitcookie\Middleware\PlainRenderingMiddleware::class,
			'description' => '',
			'after' => [
				'typo3/cms-frontend/content-length-headers'
			],
		],
	],
];