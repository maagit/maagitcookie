<?php
	return [
		'extensions-maagitcookie_consent' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitcookie/Resources/Public/Icons/cookieconsent.png'
		]
	];
?>