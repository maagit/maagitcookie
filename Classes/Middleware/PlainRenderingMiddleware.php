<?php
namespace Maagit\Maagitcookie\Middleware;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Middleware
	class:				PlainRenderingMiddleware

	description:		remove cookie consent relevant code

	created:			2022-12-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-08	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PlainRenderingMiddleware implements \Psr\Http\Server\MiddlewareInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitcookie\General;


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Get body and block cookie relevant code
     *
     * @param	\Psr\Http\Message\ServerRequestInterface		$request			the request
	 * @param	\Psr\Http\Server\RequestHandlerInterface		$handler			the request handler
	 * @return	\Psr\Http\Message\ResponseInterface									the response
     */
	public function process(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Server\RequestHandlerInterface $handler): \Psr\Http\Message\ResponseInterface
    {
		// return, if there is called "/sitemap.xml"
		$params = $request->getServerParams();
		if (strtolower(substr($params['REQUEST_URI'], 0, 12)) == '/sitemap.xml')
		{
			return $handler->handle($request);
		}

		// get current response
		$response = $handler->handle($request);
		if ($response instanceof \TYPO3\CMS\Core\Http\NullResponse)
		{
			return $response;
		}

		// extract the content
		$body = $response->getBody();
		$body->rewind();
		$content = $response->getBody()->getContents();

		// block cookie relevant code
		$categoryRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcookie\Domain\Repository\CategoryRepository::class);
		$categories = $categoryRepository->findAll();
		if (empty($categories))
		{
			return $response;
		}

		// loop categories and make cookie restrictions
		foreach ($categories as $category)
		{
			foreach ($category->getServices() as $service)
			{
				if (!$service->getAllowed() && !empty($service->getPattern()))
				{
					$content = $this->executeReplacement($content, $service->getPattern(), $service->getReplacement());
				}
			}
		}

		// push new content back into the response
		$body = new \TYPO3\CMS\Core\Http\Stream('php://temp', 'rw');
		$body->write($content);
		return $response->withBody($body);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Search pattern and replace it with given replacement
     *
     * @param	string									$content				the content to search in
	 * @param	string									$pattern				the regular expression
	 * @param	string									$replacement			the replacement string
	 * @return	string															the new content
     */
	protected function executeReplacement(string $content, string $pattern, string $replacement): string
	{
		return preg_replace($pattern, $replacement, $content);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
