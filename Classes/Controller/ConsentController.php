<?php
namespace Maagit\Maagitcookie\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Controller
	class:				ConsentController

	description:		Display cookie consent bar.

	created:			2020-12-09
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-09	Urs Maag		Initial version
						2024-09-19	Urs Maag		Method "renderAssetsForRequest"
													with try/catch block, insted of
													checking view of "TemplateView"
													instance

------------------------------------------------------------------------------------- */


class ConsentController extends \Maagit\Maagitcookie\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
     *
     * @return void
     */
    public function showAction()
    {
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Additional variables to header asset
     *
     * @return void
     */
	protected function renderAssetsForRequest($request): void
	{
		try {
			$categoryRepository = $this->makeInstance('Maagit\Maagitcookie\Domain\Repository\CategoryRepository');
			$categories = $categoryRepository->findAll();
			$pageRenderer = $this->makeInstance('TYPO3\CMS\Core\Page\PageRenderer');
		    $variables = ['request' => $request, 'arguments' => $this->arguments, 'categories' => $categories];
		    $headerAssets = $this->view->renderSection('HeaderAssets', $variables, true);
		    $footerAssets = $this->view->renderSection('FooterAssets', $variables, true);
		    if (!empty(trim($headerAssets))) {
		        $pageRenderer->addHeaderData($headerAssets);
		    }
		    if (!empty(trim($footerAssets))) {
		        $pageRenderer->addFooterData($footerAssets);
		    }
		}
		catch (\Exception $ex)
		{
			return;
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
?>