<?php
namespace Maagit\Maagitcookie\Helper;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Helper
	class:				DivHelper

	description:		Various helper methods for this plugin.

	created:			2022-12-10
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-10	Urs Maag		Initial version
						2023-12-13	Urs Maag		Added attribute "site" to request
													on getExtbaseRequest()

------------------------------------------------------------------------------------- */


class DivHelper extends \Maagit\Maagitcookie\Helper\BaseHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * execute a object method by reflection
     *
     * @param	object		$object			the object to call the method
	 * @param	string		$method			the method name
	 * @param	array		$params			the method parameters
	 * @return	mixed						the method call result
     */
	public function callByReflection(object $object, string $method, array $args)
	{
		$reflectionMethod = new \ReflectionMethod($object, $method);
		$reflectionMethod->setAccessible(true);
		return $reflectionMethod->invokeArgs($object, $args);
	}
	
	/**
     * execute a object method by reflection
     *
     * @param	object		$object			the object to call the method
	 * @param	string		$member			the member name
	 * @return	mixed						the member variable
     */
	public function getMemberByReflection(object $object, string $member)
	{
		$reflectionClass = new \ReflectionClass($object);
		$reflectionProperty = $reflectionClass->getProperty($member);
		$reflectionProperty->setAccessible(true);
		return $reflectionProperty->getValue($object);
	}
	
	/**
     * create a uri
     *
     * @param	int			$pid			the target page uid
	 * @param	array		$params			the url parameters
	 * @param	boolean		$absolute		create a absolute uri?
	 * @param	boolean		$clearCHash		clear cHash parameter?
	 * @return	string						the generated uri
     */
	public function getUri(int $pid, array $params, bool $absolute=false, bool $clearCHash=false)
	{
		// create link
		$uriBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Mvc\\Web\\Routing\\UriBuilder');
		$uriBuilder->setRequest($this->getExtbaseRequest());
		$uri = $uriBuilder->setTargetPageUid($pid)->setArguments($params)->build();
		
		// clear cHash parameter
		if ($clearCHash)
		{
			$uri = (strpos($uri, '&cHash=')===FALSE) ? $uri : substr($uri, 0, strpos($uri, '&cHash='));
		}
		
		// make a absolute uri
		if ($absolute)
		{
		    $request = $this->getRequest();
			$host = $_SERVER['HTTP_HOST'];
			$protocol = $request->getServerParams()['REQUEST_SCHEME'].'://';
			$uri = $protocol.$host.$uri;
		}
		
		// return generated uri
		return $uri;
	}
	
	/**
     * extracts the only name of a given controller object (without "Controller" suffix)
     *
     * @param	\Maagit\Maagitcookie\Controller\BaseController		$controller			the controller object
	 * @return	string																	the name of controller
     */
	public function getControllerName(\Maagit\Maagitcookie\Controller\BaseController $controller)
	{
		$className = get_class($controller);
		$name = strtolower(str_replace('Controller', '', substr($className, strrpos($className, '\\') + 1)));
		return $name;
	}
	
	/**
     * translate repository name to model name
     *
     * @param	\Maagit\Maagitcookie\Domain\Repository\BaseRepository		$repository		the repository object
	 * @return	string																		the model name
     */
	public function getModelName(\Maagit\Maagitcookie\Domain\Repository\BaseRepository $repository)
	{
		$name = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateRepositoryNameToModelName(get_class($repository));
		return $name;
	}

	/**
     * create a core request object
     *
     * @param	-
	 * @return	\TYPO3\CMS\Core\Http\ServerRequestInterface					the request object
     */
	public function getRequest()
	{
		return \TYPO3\CMS\Core\Http\ServerRequestFactory::fromGlobals();
	}

	/**
     * create a extbase request object
     *
     * @param	string								$controller		the controller class as string
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getExtbaseRequest(string $controller='\Maagit\Maagitcookie\Controller\ConsentController')
	{
		$siteFinder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Site\\SiteFinder');
		$sites = $siteFinder->getAllSites();
		$site = empty($sites['default']) ? ($sites['config'] ?? array_values($sites)[0]) : $sites['default'];
		$extbaseAttribute = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters::class, '\Maagit\Maagitproduct\Controller\CheckoutController');
	    $extbaseRequest = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\Request::class, $this->getRequest()->withAttribute('extbase', $extbaseAttribute)->withAttribute('applicationType', 1)->withAttribute('site', $site));
		return $extbaseRequest;
	}

	/**
     * create a rendering context
     *
     * @param	-
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getRenderingContext()
	{
	    $renderingContext = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextFactory::class)->create();
	    $renderingContext->setRequest($this->getExtbaseRequest());
		return $renderingContext;
	}

	/**
     * get current request host
     *
     * @param	-
	 * @return	string										the request host
     */
	public function getRequestHost()
	{
	    $request = $this->getRequest();
		$requestHost = $request->getServerParams()['REQUEST_SCHEME'].'://'.$request->getServerParams()['HTTP_HOST'];
		return $requestHost;
	}

	/**
     * get parameters from a given url string
     *
     * @param	string					$url				the url
	 * @return	array										the arguments as key/value pairs
     */
	public function getUrlParameter(string $url)
	{
		preg_match_all('/([^?&=#]+)=([^&#]*)/', $url, $result);
		$arguments = array_combine($result[1], $result[2]);
		return $arguments;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
