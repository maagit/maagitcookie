<?php
namespace Maagit\Maagitcookie\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Repository
	class:				PageRepository

	description:		Repository for the typo3 pages.

	created:			2022-10-11
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-10-11	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PageRepository extends \Maagit\Maagitcookie\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get page id's from page tree
     *
	 * @param	array			$uids					the root page uid(s)
	 * @param	int				$level					the max. tree level
	 * @return	array									the page uid's
     */
	public function getPageTreeUids(array $uids, int $level=0)
	{
		$pages = array();
		foreach ($uids as $uid)
		{
			$pages = array_merge($this->_getPagesRecursive($uid, $level), $pages);
		}
		return $pages;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get page id's, selected recursively from page tree
     *
	 * @param	int			$uid					the current page uid
	 * @param	int			$level					the max. tree level
	 * @param	int			$currentLevel			the current tree level
	 * @return	array								the page uid's
     */
	private function _getPagesRecursive(int $uid, int $level=0, int $currentLevel=0)
	{
		$pages = array($uid);
		if ($currentLevel < $level)
		{
			$currentLevel++;
			$pageRepository = $this->makeInstance('TYPO3\\CMS\\Core\\Domain\\Repository\\PageRepository');
			$menuItems = $pageRepository->getMenu($uid, 'uid', '', 'sorting', false);
			foreach ($menuItems as $key => $value)
			{
				$pages = array_merge($pages, $this->_getPagesRecursive($key, $level, $currentLevel));
			}
		}
		return $pages;
	}
}
