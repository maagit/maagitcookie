<?php
namespace Maagit\Maagitcookie\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Repository
	class:				BaseRepository

	description:		Base class for Repository classes.
						Inherits the method "create" to create belonging model.

	created:			2022-12-10
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-10	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class BaseRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitcookie\General;


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		// instantiate persistenceManager ()
		$this->persistenceManager = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitcookie\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$modelName = $this->divHelper->getModelName($this);
		$domainModel = $this->makeInstance($modelName, $arguments);
		return $domainModel;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}