<?php
namespace Maagit\Maagitcookie\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Repository
	class:				CookieconsentcategoryRepository

	description:		Repository for the cookie consent category model.

	created:			2022-12-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-08	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CategoryRepository extends \Maagit\Maagitcookie\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * Default ordering
     */
	protected $defaultOrderings = array(
		'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
		'services.sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Initialize the object, set default values
     *
     */
	public function initializeObject()
	{
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitcookie\Domain\Model\Category';

		// set default query settings
		$querySettings = $this->makeInstance('TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings');
		if (empty($this->settings['consent']['storagePid']))
		{
			$querySettings->setRespectStoragePage(false);
		}
		else
		{
			$pageRepository = $this->makeInstance('Maagit\Maagitcookie\Domain\Repository\PageRepository');
			$pidList = $pageRepository->getPageTreeUids(
				\TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $this->settings['consent']['storagePid'], true),
				(!empty($this->settings['consent']['recursive'])) ? $this->settings['consent']['recursive'] : 0
			);
			$querySettings->setRespectStoragePage(true);
			$querySettings->setStoragePageIds($pidList);
		}
		$this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Get service objects
     *
     * @param	-
	 * @return	\TYPO3\CMS\Extbase\Persistence\QueryResultInterface				the query result
     */
	public function findAll()
	{
		if (empty($this->settings['consent']['storagePid'])) {return null;}
		$objects = parent::findAll();
		foreach($objects as $object)
		{
			foreach($object->getServices() as $service)
			{
				$service->setAllowed($this->isAllowed($service));	
			}
		}
		return $objects;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Check cookie consent cookie and return validation of service
     *
     * @param	\Maagit\Maagitcookie\Domain\Model\Service	$service				the requested service
	 * @return	boolean																is it allowed?
     */
	public function isAllowed(\Maagit\Maagitcookie\Domain\Model\Service $service)
	{
		if (!isset($_COOKIE['cconsent']))
		{
			return false;
		}
		if ($service->getCategory()->getNeeded())
		{
			return true;
		}
		$cookie = json_decode($_COOKIE['cconsent'], true);
		if (isset($cookie['categories'][$service->getCategory()->getKey()]['wanted']) && $cookie['categories'][$service->getCategory()->getKey()]['wanted'])
		{
			return true;
		}
		return false;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}