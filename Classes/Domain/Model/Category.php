<?php
namespace Maagit\Maagitcookie\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Model
	class:				Category

	description:		Model for the Cookieconsent category.
						Inherits datas of the cookieconsent category.

	created:			2022-12-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-08	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Category extends \Maagit\Maagitcookie\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $needed;

    /**
     * @var boolean
     */
    protected $wanted;

    /**
     * @var boolean
     */
    protected $checked;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitcookie\Domain\Model\Service>
     */
    protected $services;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Return the key
     *
     * @return string	$key
     */
    public function getKey()
    {
		return 'category_'.$this->uid;
    }

	/**
     * Set the name
     *
     * @param string	$name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Return the name
     *
     * @return string	$name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the description
     *
     * @param string 	$description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Return the description
     *
     * @return string	$description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the needed
     *
     * @param boolean 	$needed
     */
    public function setNeeded($needed)
    {
        $this->needed = $needed;
    }

    /**
     * Return the needed
     *
     * @return boolean	$needed
     */
    public function getNeeded()
    {
        return $this->needed;
    }
	
    /**
     * Sets the wanted
     *
     * @param boolean 	$wanted
     */
    public function setWanted($wanted)
    {
        $this->wanted = $wanted;
    }

    /**
     * Return the wanted
     *
     * @return boolean	$wanted
     */
    public function getWanted()
    {
        return $this->wanted;
    }

    /**
     * Sets the checked
     *
     * @param boolean 	$checked
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;
    }

    /**
     * Return the checked
     *
     * @return boolean	$checked
     */
    public function getChecked()
    {
        return $this->checked;
    }

	/**
     * Returns the services
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitcookie\Domain\Model\Service>	$services
     */
    public function getServices()
    {
        return $this->services;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
