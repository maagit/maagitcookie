<?php
namespace Maagit\Maagitcookie\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Model
	class:				BaseModel

	description:		Base class for Model classes.
						Inherits method "toArray" to serialize the model objects

	created:			2022-12-10
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-10	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class BaseModel extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitcookie\General;

	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Convert object to array
     *
     * @return array	the object as array
     */
	public function toArray()
	{
		$array = array();
		$validTypes = array('string', 'boolean', 'integer', 'double', 'array', 'object', 'NULL', 'unknown type');
		$exclude = array('settings');
		$class = $this;
		foreach ($class as $member => $value)
		{
			if (in_array(gettype($this->$member), $validTypes) && !in_array($member, $exclude))
			{
				if (gettype($this->$member) == 'object')
				{
					if ($this->$member instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage)
					{
						$i = 0;
						foreach ($this->$member as $key => $value)
						{
							if (method_exists($value, 'toArray'))
							{
								$array[$member][$i++] = $value->toArray();
							}
						}					
					}
					else
					{
						if (method_exists($this->$member, 'toArray'))
						{
							$array[$member] = $this->$member->toArray();
						}
					}
				}
				else
				{
					$propertyName = 'get'.ucfirst($member);
					if (method_exists($class, $propertyName))
					{
						$array[$member] = $this->$propertyName();
					}
				}
			}

		}
		return $array;
	}
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}