<?php
namespace Maagit\Maagitcookie\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcookie
	Package:			Model
	class:				Service

	description:		Model for the Cookieconsent service.
						Inherits datas of the cookieconsent service.

	created:			2022-12-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-12-08	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Service extends \Maagit\Maagitcookie\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $name;

	/**
     * @var \Maagit\Maagitcookie\Domain\Model\Category
     */
    protected $category;

    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var string
     */
    protected $replacement;

    /**
     * @var boolean
     */
    protected $allowed;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Return the key
     *
     * @return string	$key
     */
    public function getKey()
    {
        return 'service_'.$this->uid;
    }

	/**
     * Set the name
     *
     * @param string	$name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Return the name
     *
     * @return string	$name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the category
     *
     * @param \Maagit\Maagitcookie\Domain\Model\Category	$category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Return the category
     *
     * @return \Maagit\Maagitcookie\Domain\Model\Category	$category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the pattern
     *
     * @param string 	$oattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * Return the pattern
     *
     * @return string	$pattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }
	
    /**
     * Sets the replacement
     *
     * @param string 	$replacement
     */
    public function setReplacement($replacement)
    {
        $this->replacement = $replacement;
    }

    /**
     * Return the replacement
     *
     * @return string	$replacement
     */
    public function getReplacement()
    {
        return $this->replacement;
    }

    /**
     * Sets the allowed
     *
     * @param boolean 	$allowed
     */
    public function setAllowed($allowed)
    {
        $this->allowed = $allowed;
    }

    /**
     * Return the allowed
     *
     * @return boolean	$allowed
     */
    public function getAllowed()
    {
        return $this->allowed;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
